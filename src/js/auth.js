function getUsers() {
    return {
        'user': 'pass'
    }
}

function check(user, pass) {
    const users = getUsers()
    return users?.[user] === pass
}

function getCredentials() {
    const params = new URLSearchParams(location.search)
    const user = params.get('user')
    const pass = params.get('pass')
    return { user, pass }
}

function doLogin() {
    const { user, pass } = getCredentials()
    if (check(user, pass)) {
        location.replace('./dashboard.html')
    } else {
        location.replace('./index.html')
    }
}

doLogin()