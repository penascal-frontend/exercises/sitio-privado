function mapURL(gender, status) {
    let url = 'https://rickandmortyapi.com/api/character/'
    if (!!gender || !!status) url += '?'
    if (!!gender) url += `gender=${gender}`
    if (!!gender && !!status) url += '&'
    if (!!status) url += `status=${status}`
    return url
}

function mapCharacter(data) {
    const { id, name, status, species, type, gender, image } = data
    return { id, name, status, species, type, gender, image }
}

async function getData(url) {
    const response = await fetch(url)
    if (!response.ok) return []
    const json = await response.json()
    return json.results.map(mapCharacter)
}

function renderCharacter(character) {
    const elem = document.createElement('img')
    elem.src = character.image
    elem.alt = character.name
    return elem
}

async function updateMain() {
    const main = document.getElementById('main')
    const gender = document.querySelector('input[name=gender]:checked')?.value ?? ''
    const status = document.querySelector('input[name=status]:checked')?.value ?? ''
    const url = mapURL(gender, status)

    main.innerText = ''
    const characters = await getData(url)
    characters.forEach(character => main.appendChild(renderCharacter(character)))

}

function updateRadioDisabledState(event) {
    const checkbox = event.target
    const filter = checkbox.dataset.filter
    document.querySelectorAll(`input[name=${filter}]`).forEach(radio => { 
        if (!checkbox.checked) {
            radio.checked = false
            radio.disabled = true
        } else {
            radio.disabled = false
        }
    })
    if (!checkbox.checked) updateMain()
}

function main() {
    const inputs = {
        gender: {
            checkbox: document.querySelector('input[type=checkbox][data-filter=gender]'),
            radios: document.querySelectorAll('input[type=radio][name=gender]')
        },
        status: {
            checkbox: document.querySelector('input[type=checkbox][data-filter=status]'),
            radios: document.querySelectorAll('input[type=radio][name=status]')
        },
    }
    const checkboxes = [inputs.gender.checkbox, inputs.status.checkbox]
    const radios = [...inputs.gender.radios, ...inputs.status.radios]

    radios.forEach(radio => {
        radio.disabled = true
        radio.checked = false
        radio.addEventListener('change', updateMain)
    })

    checkboxes.forEach(checkbox => {
        checkbox.checked = false
        checkbox.addEventListener('change', updateRadioDisabledState)
    })
}

main()